#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>

#include <velib/canhw/canhw.h>
#include <velib/platform/console.h>
#include <velib/types/ve_dbus_item.h>
#include <velib/types/ve_item_def.h>
#include <velib/types/ve_values.h>
#include <velib/canhw/canhw_types.h>
#include "../velib/inc/velib/canhw/canhw_types.h"
#include "../velib/inc/velib/platform/plt.h"
#include <velib/utils/ve_item_utils.h>
#include <velib/utils/ve_timer.h>

/** How often to push values to dbus in ms? */
#define UPDATE_INTERVAL 1000

static const VeCanId PIKTRONIK_IDS[2] = { 0x64, 0x74 };

#define PIKTRONIK_STATUS1_OFFSET 2
#define PIKTRONIK_STATUS2_OFFSET 3

/** When the values were pushed to dbus. */
static un16 lastUpdate;

typedef struct {
  VeItem DeviceInstance;
  VeItem motorRPM;
  VeItem motorTemperature;
  VeItem controllerDCPower;
  VeItem controllerDCVoltage;
  VeItem controllerDCCurrent;
  VeItem controllerTemperature;
} Device;

enum ControllerType { NONE = 0, SEVCON, PIKTRONIK };

struct controller {
  bool exists;
  enum ControllerType type;
  int32_t motorRPM;
  int16_t motorTemperature;
  float controllerDCVoltage;
  float controllerDCCurrent;
  int16_t controllerTemperature;
  VeItem root;
  Device dev;
  struct VeDbus *conn;
};

static struct controller cntrls[2];

void controllerSend(size_t i);

void controllerAdd(size_t i, enum ControllerType type)
{
  VeItem *root = &cntrls[i].root;

  Device *dev = &cntrls[i].dev;
  /* Add values which should be accessible on the dbus */
  veItemAddChildByUid(root, "DeviceInstance", &dev->DeviceInstance);
  veItemAddBasic(root, "Motor/RPM", &dev->motorRPM);
  veItemAddChildByUid(root, "Motor/Temperature", &dev->motorTemperature);
  veItemAddChildByUid(root, "Dc/0/Power", &dev->controllerDCPower);
  veItemAddChildByUid(root, "Dc/0/Voltage", &dev->controllerDCVoltage);
  veItemAddChildByUid(root, "Dc/0/Current", &dev->controllerDCCurrent);
  veItemAddChildByUid(root, "Controller/Temperature", &dev->controllerTemperature);

  cntrls[i].conn = veDbusConnectString(veDbusGetDefaultConnectString());
  if (cntrls[i].conn == NULL)
  {
    printf("dbus_service: no dbus connection\n");
    pltExit(10);
  }

  /* make the values available on the dbus and get a service name */
  char *name;
  if (type == SEVCON)
  {
    if (i == 0)
      name = "com.victronenergy.motordrive.sevcon0";
    else
      name = "com.victronenergy.motordrive.sevcon1";
  }
  else
  {
    if (i == 0)
      name = "com.victronenergy.motordrive.piktronik0";
    else
      name = "com.victronenergy.motordrive.piktronik1";
  }

  veDbusItemInit(cntrls[i].conn, root);
  if (!veDbusChangeName(cntrls[i].conn, name))
  {
    printf("dbus_service: registering name failed\n");
    pltExit(11);
  }

  VeVariant variant;
  veItemOwnerSet(&cntrls[i].dev.DeviceInstance, veVariantUn32(&variant, i));

  controllerSend(i);
  cntrls[i].type = type;
  cntrls[i].exists = true;
}

void sevconUpdate(size_t i, const VeRawCanMsg *msg)
{
  if ((msg->canId == 0x132 + i) && (msg->length == 8))
  {
    if (!cntrls[i].exists)
      controllerAdd(i, SEVCON);
    if (cntrls[i].type == SEVCON)
      cntrls[i].motorRPM = (msg->mdata[7] << 24) | (msg->mdata[6] << 16) | (msg->mdata[5] << 8) | msg->mdata[4];
  }
  else if ((cntrls[i].type == SEVCON) && (msg->canId == 0x162 + i) && (msg->length >= 5))
  {
    cntrls[i].controllerDCVoltage = (float)((signed short)(msg->mdata[1] << 8) | msg->mdata[0]) * 0.0625;
    cntrls[i].controllerDCCurrent = (float)((signed short)(msg->mdata[4] << 8) | msg->mdata[3]) * 0.0625;
    cntrls[i].controllerTemperature = (int32_t)(signed short)(msg->mdata[2]);
  }
  else if ((cntrls[i].type == SEVCON) && (msg->canId == 0x248 + i) && (msg->length >= 4))
  {
    cntrls[i].motorTemperature = (int16_t)((signed short)(msg->mdata[3] << 8) | msg->mdata[2]);
  }
}

void piktronikUpdate(size_t i, const VeRawCanMsg *msg)
{
  if ((msg->canId == PIKTRONIK_IDS[i] + PIKTRONIK_STATUS1_OFFSET) && (msg->length == 8))
  {
    if (!cntrls[i].exists)
      controllerAdd(i, PIKTRONIK);
    if (cntrls[i].type == PIKTRONIK)
    {
      cntrls[i].controllerDCVoltage = (float)((msg->mdata[2] << 8) + msg->mdata[3]) / 100.0f;
      cntrls[i].motorRPM = (int)((((msg->mdata[0] << 8) + msg->mdata[1]) - 0x8000) / 3.27701f);
    }
  }
  else if ((cntrls[i].type == PIKTRONIK) && (msg->canId == PIKTRONIK_IDS[i] + PIKTRONIK_STATUS2_OFFSET) && (msg->length == 8))
  {
    float current = 0.0f;
    if (!((msg->mdata[6] == 0xff) && (msg->mdata[7] == 0xff)))
      current = (float)(((msg->mdata[6] << 8) + msg->mdata[7]) - 0x8000) * -0.0186f;
    cntrls[i].controllerDCCurrent = current;
    cntrls[i].motorTemperature = (int)(msg->mdata[0]) - 54;
    cntrls[i].controllerTemperature = (int)(msg->mdata[1]) - 54;
  }
}

void controllerUpdate(size_t i, const VeRawCanMsg *msg)
{
  switch (cntrls[i].type)
  {
  case NONE:
    sevconUpdate(i, msg);
    piktronikUpdate(i, msg);
    break;
  case SEVCON:
    sevconUpdate(i, msg);
    break;
  case PIKTRONIK:
    piktronikUpdate(i, msg);
    break;
  }
}

void controllerSend(size_t i)
{
  VeVariant variant;
  Device *dev = &cntrls[i].dev;
  veItemOwnerSet(&dev->motorRPM, veVariantSn32(&variant, cntrls[i].motorRPM));
  veItemOwnerSet(&dev->motorTemperature, veVariantSn16(&variant, cntrls[i].motorTemperature));
  const float power = cntrls[i].controllerDCVoltage * cntrls[i].controllerDCCurrent;
  veItemOwnerSet(&dev->controllerDCPower, veVariantFloat(&variant, power));
  veItemOwnerSet(&dev->controllerDCVoltage, veVariantFloat(&variant, cntrls[i].controllerDCVoltage));
  veItemOwnerSet(&dev->controllerDCCurrent, veVariantFloat(&variant, cntrls[i].controllerDCCurrent));
  veItemOwnerSet(&dev->controllerTemperature, veVariantSn16(&variant, cntrls[i].controllerTemperature));
}

/* Called after CAN is already setup */
void taskInit(void)
{
}

void taskUpdate(void)
{
  VeRawCanMsg msg;

  while (veCanRead(&msg))
  {
    /* ignore all 29 bit / extended messages */
    /* if (msg.flags & VE_CAN_EXT) */
    /*   continue; */

    controllerUpdate(0, &msg);
    controllerUpdate(1, &msg);
  }

  if (veTick1ms(&lastUpdate, UPDATE_INTERVAL))
  {
    if (cntrls[0].exists)
      controllerSend(0);
    if (cntrls[1].exists)
      controllerSend(1);
  }
  /* printf("msg id:%X dlc:%d flags: %d", msg.canId, msg.length, msg.flags);
	 for (n = 0; n < msg.length; n++)
	 printf(" %02X", msg.mdata[n]);
	 putchar('\n'); */

  /* printf("Motor rpm: %d voltage: %.1f current: %.1f conttemp: %d motortemp: %d\n", motorRPM, controllerDCVoltage, controllerDCCurrent, controllerTemperature, motorTemperature); */
}

/* 50ms time progress */
void taskTick(void)
{

}
